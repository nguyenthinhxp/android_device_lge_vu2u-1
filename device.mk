#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This file includes all definitions that apply to ALL geefhd devices, and
# are also specific to geefhd devices
#
# Everything in this directory will become public

$(call inherit-product, device/lge/vu2u-common/vu2u.mk)

DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay

# This device is xhdpi.  However the platform doesn't
# currently contain all of the bitmaps at xhdpi density so
# we do this little trick to fall back to the hdpi version
# if the xhdpi doesn't exist.
PRODUCT_AAPT_CONFIG := normal hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := xhdpi
PRODUCT_LOCALES += ko_KR xhdpi

PRODUCT_COPY_FILES += \
        $(LOCAL_PATH)/mixer_paths.xml:system/etc/mixer_paths.xml.nothing

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/thermald-8960.conf:system/etc/thermald-8960.conf \
	$(LOCAL_PATH)/thermal-engine-8960.conf:system/etc/thermal-engine-8960.conf
#Light
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/liblight/lights.msm8960.so:system/lib/hw/lights.msm8960.so

#RamDisk
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.fx1sk.rc:root/init.fx1sk.rc \
	$(LOCAL_PATH)/fstab.fx1sk:root/fstab.fx1sk \
	$(LOCAL_PATH)/ueventd.fx1sk.rc:root/ueventd.fx1sk.rc

# Recovery
PRODUCT_COPY_FILES += \
        $(LOCAL_PATH)/recovery/postrecoveryboot.sh:recovery/root/sbin/postrecoveryboot.sh

#Keylayout
PRODUCT_COPY_FILES += \
       $(LOCAL_PATH)/keypad_8064.kl:system/usr/keylayout/gk-keypad-8064.kl

#Wifi
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/bcmdhd.cal:system/etc/wifi/bcmdhd.cal

#LCD Destiny
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.lcd_density=280

PRODUCT_PACKAGES += \
	hwaddrs \
        hci_qcomm_init \
        f2fs-tools \
        exfat \
        ffmpeg \
        naver-fonts

PRODUCT_PACKAGES += \
	brcm_patchram_plus

PRODUCT_PROPERTY_OVERRIDES += \
	ro.bt.bdaddr_path=/data/misc/bdaddr

# This hw ships locked, work around it with loki
PRODUCT_PACKAGES += \
	recovery-transform.sh

PRODUCT_COPT_FILES += \
	frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml

PRODUCT_PROPERTY_OVERRIDES += \
	ro.telephony.default_network=9 \
	telephony.lteOnGsmDevice=1 \
        ro.hwui.text_cache_width=2048 \
        ro.qualcomm.cabl=1 \
        persist.gps.qmienabled=true \
        ro.vendor.extension_library=/system/lib/libqc-opt.so \
        ro.use_data_netmgrd=true \
        persist.data.netmgrd.qos.enable=false 

$(call inherit-product, frameworks/native/build/phone-xhdpi-1024-dalvik-heap.mk)
$(call inherit-product-if-exists, hardware/broadcom/wlan/bcmdhd/config/config-bcm.mk)
